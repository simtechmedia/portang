var app = angular.module("portfolio", ['ui','ngSanitize'] , function($routeProvider, $locationProvider) {
    // App Config
        $routeProvider
        .when('/', {
            templateUrl: 'frontitems.html',
            controller: PortFrontCtrl
        })
        .when('/portitem/:id',{
            templateUrl:'frontitem.html',
            controller: PortItemCtrl
        });

    });

    app.directive("frontitem", function(){
    return {
        restrict : "E",
        template : "<div class=\"ic_container\">\n    <img src=\"images/{{frontitem.picturl}}\" width=\"154\" height=\"137\">\n    <div class=\"overlay\" style=\"display:none;\"></div>\n    <div class=\"ic_caption\">\n        <p class=\"ic_text\">\n            {{frontitem.name}}\n        </p>\n    </div>\n</div>\n"
    }}
)

function PortItemCtrl($scope, $routeParams, $http)
{
    $scope.model = {
        message : $routeParams.id
    }

    // Gets Project Information
    $http.get('http://localhost/portdata/?q=getproject&projectID='+$routeParams.id).success(function(data) {
        $scope.data = data;
        $scope.htmlinfo = data[0].description.replace(new RegExp("<br\/>", "g"), '');
    });

    $http.get('http://localhost/portdata/?q=getimages&projectID='+$routeParams.id).success(function(imageData) {
        $scope.imagesdata = imageData;
        setTimeout(function(){
            fleXenv.initByClass("fullinfo");
        },100);
    });
}

function PortListCtrl($scope, $http) {
    // Static Pages
    $scope.staticPages = [
        {name: 'Home'} ,
        {name: 'About me / Contact'} ,
        {name : 'My Blog'}
    ];

    $scope.sphighLight = function(staticPage)
    {
        $scope.spselected = staticPage;
    }

    $scope.spunhighlight = function()
    {
        $scope.spselected = null;
    }

    $scope.spisSelected = function(staticPage) {
        return $scope.spselected === staticPage;
    }


    // Port Items
    $scope.portItems;
    $http.get('http://localhost/portdata/?q=getall').success(function(data) {
        $scope.portitems = data;
    });

    // Highlight for buttons
    $scope.highLight = function(portItem)
    {
        console.log(portItem);
        $scope.selected = portItem;
    }

    $scope.unhighlight = function()
    {
        $scope.selected = null;
    }

    $scope.isSelected = function(portItem) {
        return $scope.selected === portItem;
    }


}

function PortFrontCtrl($scope, $http) {
    $http.get('http://localhost/portdata/?q=getfront').success(function(data) {
        $scope.frontitems = data;

        setTimeout(function(){
            fleXenv.fleXcrollMain("menuCol");
            $(".ic_container").capslide({
                caption_color	: 'white',
                caption_bgcolor	: '#f15a24',
                overlay_bgcolor : 'black',
                border			: '',
                showcaption	    : false
            });
        },100);
    });
}
